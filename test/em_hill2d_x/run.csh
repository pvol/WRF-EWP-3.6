#!/bin/csh
#PBS -N RST_U08
#PBS -l nodes=1:ppn=1
#PBS -r n
#PBS -l walltime=40:00:00
#PBS -m e
#PBS -o /mnt/mimer/pvol/WRFV3WF/test/INF/U08/rst/turbine.out
#PBS -e /mnt/mimer/pvol/WRFV3WF/test/INF/U08/rst/turbine.err
#PBS -q workq
#PBS -V

module unload hdf5
module unload netcdf

module load hdf5/1.8.12_pgi-13.10
module load netcdf/4.3.1.1_pgi-13.10

# ------------------------------------------------------------------------------
# set directory manually
cd /home/pvol/WRFV36WF/test/em_hill2d_x

./ideal.exe


cp namelistrst.input namelist.input

./wrf.exe

rm wrfout_d01_0001-01-01_00:00:00

cp namelist_ewp.input namelist.input

./wrf.exe

rm wrfrst_d01_0001-01-02_00:00:00

exit

