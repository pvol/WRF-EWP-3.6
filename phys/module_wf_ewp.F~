Module module_wf_ewp

!!! By P. Volker, DTU Windenergy
!!! Explicit Wake Parametrisation (EWP) V1.0
!!! The Explicit Wake Parametrisation: Development, implementation in a mesoscale
!!! model and evaluation against measurements, Geosci. Model Dev.

!!! This module is suitable for the Serial, Shared or Distributed Memory option
!!! When activated in the namelist, this module is called from the pbl_physics routine:

!!! (1) Initialisation (TURBINE_INIT): 
!!!     It reads the turbine positions and turbine type from the in the namelist 
!!!     specified file
!!! (2) Wake calculation (TURBINE_SELF_PRESERV):
!!!     calculates the deceleration for every specified turbine and applis it to 
!!!     the WRF grid-cells

  USE module_configure, ONLY : grid_config_rec_type

  USE module_model_constants, only: piconst
       


  IMPLICIT NONE


  INTEGER, PARAMETER :: WIND_TURBINES_OFF      = 0 ,&
       &                WIND_TURBINES_IDEAL    = 1 ,&
       &                WIND_TURBINES_REAL     = 2
  
  REAL  , PARAMETER   ::                                     &
       z0         = 0.0 ,       z1         = 1.0            ,&
       z2         = 2.0 ,       z3         = 3.0            ,&
       piGr       = piconst,    z1d2       = z1/z2          ,&    
       z2d3       = z2/z3,      z3d2       = z3/z2          ,&     
       eps        = 1e-3,       large      = 1e6            ,&  
       sqrt2      = 1.41421356, sqrt_pi    = 1.772453851    ,&
       undef      = -9999.,    KMmin      = 3.0             ,&      
       Rearth     = 6371000.,   D2R        = piGr/180.      ,&
       Betz       = 16./27.,    sqrt8      = 2.83           ,&
       rhoA       = 1.224


  INTEGER , PARAMETER ::                                     &
       n0       = 0 ,            n1     = 1                 ,&
       n2       = 2 ,            n5     = 5                 ,&
       error    = 999

  CHARACTER(len=256) , PARAMETER ::   turb_type = 'Specified'

  LOGICAL , EXTERNAL      :: wrf_dm_on_monitor

  !In V1.1 exchange the integer cluster_nr to the real Nturbines (number of turbines)
  !Every turbine has the properties:
  TYPE turbine_specs
     INTEGER                            :: i, j      ! x and y coords of turbines
     REAL                               :: hubheight ! height (H) of the turbine hub (m)
     REAL                               :: Nturbs    ! Number of specified turbines at a given location 
     REAL                               :: D0        ! H of the turbine hub (m)
     REAL   , DIMENSION(:), ALLOCATABLE :: Ut        ! Thrust curve U
     REAL   , DIMENSION(:), ALLOCATABLE :: Ct        ! Thrust curve Ct
     REAL   , DIMENSION(:), ALLOCATABLE :: Cp        ! Thrust curve Cp
     INTEGER                            :: turb_nr   ! which turbines are in cell(n)
     REAL                               :: Ct_stat
     REAL                               :: diameter    
     REAL                               :: cutinspeed  
     REAL                               :: cutoutspeed 
  END TYPE turbine_specs

  !Cells are used to apply the accumulative Tc
  TYPE cell_specs
     INTEGER                            :: i, j        ! x and y coords of turbines
     INTEGER                            :: turb_p_cell ! nr of turbines in cell(n)
     INTEGER, DIMENSION(:), ALLOCATABLE :: turb_nr     ! which turbines are in cell(n)
  END TYPE cell_specs

  INTEGER                               :: nturbines,ncells,nCt

  TYPE(cell_specs)   , DIMENSION(:), ALLOCATABLE :: cells
  TYPE(turbine_specs), DIMENSION(:), ALLOCATABLE :: turbines
      
  !THIS IS NEEDED ONLY FOR THE ALLOCATION OF THE SLAVE CORES
  INTEGER   , DIMENSION(:), ALLOCATABLE :: TPC_CNT !DIMENSION NCELLS

  PUBLIC  TURBINE_EWP
  PUBLIC  INIT_EWP

  PRIVATE WAKE         ,&
       &  PREP_TURB    ,&
       &  PREP_CELL    ,&
       &  GET_SPEC     ,&
       &  ABORT

 CONTAINS
    
  ! Main routines:
  ! - TURBINE_SELF_PRESERV
  ! - WAKE
  ! Utility routines:
  ! - TURBINE_INIT
  ! - PREP_TURB
  ! - PREP_CELL
  ! - GET_WIND_DIR
  ! - ABORT

!!! #########################################################################
!!! ######################### BEGIN PUBLIC ROUTINES #########################
!!! #########################################################################

!!! #########################################################################
!!!                                  (1)                                  !!!
!!! #########################################################################    
  SUBROUTINE INIT_EWP(dx,out_turb,config_flags,                      &
         &            lat,lon,                                       &
         &            IDS,IDE,JDS,JDE,KDS,KDE,                       &
         &            IMS,IME,JMS,JME,KMS,KME,                       &
         &            ITS,ITE,JTS,JTE,KTS,KTE)

    INTEGER,INTENT(IN)           :: IDS,IDE,JDS,JDE,KDS,KDE,         &
         &                          IMS,IME,JMS,JME,KMS,KME,         &
         &                          ITS,ITE,JTS,JTE,KTS,KTE,out_turb

    REAL,     DIMENSION( ims:ime , jms:jme ) , INTENT(IN) :: lon,lat
    !REAL    ,DIMENSION(ims:ime,kms:kme,jms:jme),INTENT(IN) :: dz
    !REAL    ,DIMENSION(kms:kme),INTENT(IN) :: zfull
    REAL    ,                   INTENT(IN) :: dx

    CHARACTER(len=256)           :: message
    INTEGER                      :: ITF,JTF,KTF,i,j
!    REAL                         :: known_lat,known_lon

   TYPE (grid_config_rec_type) :: config_flags


    JTF=MIN0(JTE,JDE-n1)
    KTF=MIN0(KTE,KDE-n1)
    ITF=MIN0(ITE,IDE-n1)

    write(message,*)'INSIDE TURBINE',wrf_dm_on_monitor(),JTS,JTF,ITS,ITF
    CALL wrf_debug(200,message)

    IF(.not.ALLOCATED(turbines))THEN


       IF (wrf_dm_on_monitor() ) THEN
 
!        known_lat = lat(its,jts)
!        known_lon = lon(its,jts)

!       CALL PREP_TURB( dz=dz(ITF,KTS:KTF,JTF),dx=dx,out_turb=out_turb,&
          CALL PREP_TURB(dx=dx,out_turb=out_turb                        ,&
               &         known_lat=lat(its,jts),known_lon=lon(its,jts)  ,&
               &         ITS=ITS,JTS=JTS,                                &
               &         KTS=KTS,KTF=KTF,IMS=IMS,IME=IME,JMS=JMS,JME=JME,&
               &         config_flags=config_flags)

          CALL PREP_CELL(kms=kms,kme=kme,ims=ims,ime=ime,jms=jms,jme=jme,&
               &         JTS=JTS,JTF=JTF,ITS=ITS,ITF=ITF,KTS=KTS,KTF=KTF,&
               &         dx=dx                          )
       END IF
      
       CALL wrf_dm_bcast_integer(nturbines,1) 
       CALL wrf_dm_bcast_integer(nCt      ,1)
       CALL wrf_dm_bcast_integer(ncells   ,1)

       write(message,*)'Define nturbine ',nturbines,ncells
       CALL wrf_message(message)

       IF(.NOT.wrf_dm_on_monitor() )THEN
          IF(.NOT.ALLOCATED(turbines) )THEN
             ALLOCATE(turbines(nturbines))
             DO i = 1, nturbines
                IF(.NOT.ALLOCATED(turbines(i)%Ut))&
                        ALLOCATE( turbines(i)%Ut(nCt))
                IF(.NOT.ALLOCATED(turbines(i)%Ct))&
                        ALLOCATE( turbines(i)%Ct(nCt))
                IF(.NOT.ALLOCATED(turbines(i)%Cp))&
                        ALLOCATE( turbines(i)%Cp(nCt))
             END DO
          END IF
       END IF 

       DO i = 1, nturbines 
          CALL wrf_dm_bcast_integer(turbines(i)%i          ,1) 
          CALL wrf_dm_bcast_integer(turbines(i)%j          ,1) 
          CALL wrf_dm_bcast_real(   turbines(i)%hubheight  ,1) 
          CALL wrf_dm_bcast_real(   turbines(i)%Nturbs     ,1) 
          CALL wrf_dm_bcast_real(   turbines(i)%D0         ,1) 
          DO j = 1, nCt
             CALL wrf_dm_bcast_real(turbines(i)%Ct(j)      ,1) 
             CALL wrf_dm_bcast_real(turbines(i)%Cp(j)      ,1) 
             CALL wrf_dm_bcast_real(turbines(i)%Ut(j)      ,1)
          END DO
          CALL wrf_dm_bcast_integer(turbines(i)%turb_nr    ,1) 
          CALL wrf_dm_bcast_real(   turbines(i)%Ct_stat    ,1) 
          CALL wrf_dm_bcast_real(   turbines(i)%diameter   ,1) 
          CALL wrf_dm_bcast_real(   turbines(i)%cutinspeed ,1) 
          CALL wrf_dm_bcast_real(   turbines(i)%cutoutspeed,1) 
       ENDDO

       IF(.NOT.wrf_dm_on_monitor() )THEN
          IF(.NOT.ALLOCATED(cells  ))ALLOCATE(cells(  ncells))
          IF(.NOT.ALLOCATED(TPC_CNT))ALLOCATE(TPC_CNT(ncells))
       END IF

       DO i = 1, ncells
          CALL wrf_dm_bcast_integer(TPC_CNT(i),1)
       END DO

       IF(.NOT.wrf_dm_on_monitor() )THEN
          DO i = 1,ncells
             IF(.NOT.ALLOCATED(cells(i)%turb_nr))&
                ALLOCATE(cells(i)%turb_nr(TPC_CNT(i)))
          END DO
       END IF

       DO i = 1, ncells
          CALL wrf_dm_bcast_integer(cells(i)%i             ,1)
          CALL wrf_dm_bcast_integer(cells(i)%j             ,1)
          CALL wrf_dm_bcast_integer(cells(i)%turb_p_cell   ,1)
          DO j=1,TPC_CNT(i)
             CALL wrf_dm_bcast_integer(cells(i)%turb_nr(j) ,1)
          END DO
       END DO
    END IF


  END SUBROUTINE INIT_EWP
!!! #########################################################################
!!! #########################################################################



!!!
!!!
!!!
!!! #########################################################################
!!!                                  (2)                                  !!!
!!! #########################################################################   
  SUBROUTINE TURBINE_EWP(u,v,dudt,dvdt,dx,dz,z,hgt,rho,out_turb,R0frac,  &
       &             k_m,Tmeas,production,                               &
       &             IDS,IDE,JDS,JDE,KDS,KDE,                            &
       &             IMS,IME,JMS,JME,KMS,KME,                            &
       &             ITS,ITE,JTS,JTE,KTS,KTE)
 
    
    IMPLICIT NONE

    CHARACTER(len=256),INTENT(IN)::   production

    INTEGER,INTENT(IN)           :: IDS,IDE,JDS,JDE,KDS,KDE,           &
         &                          IMS,IME,JMS,JME,KMS,KME,           &
         &                          ITS,ITE,JTS,JTE,KTS,KTE,out_turb
    
    REAL    ,DIMENSION(ims:ime,kms:kme,jms:jme),INTENT(INOUT):: k_m
    REAL    ,DIMENSION(ims:ime,jms:jme),INTENT(INOUT):: Tmeas
    REAL    ,DIMENSION(ims:ime,jms:jme),INTENT(IN)   :: hgt
    REAL    ,DIMENSION(ims:ime,kms:kme,jms:jme),INTENT(INOUT):: dudt,dvdt  
    REAL    ,DIMENSION(ims:ime,kms:kme,jms:jme),INTENT(IN   ):: U,V,z,dz,rho
    REAL    ,                                   INTENT(IN   ):: dx,R0frac

    REAL    ,DIMENSION(        kms:kme        )              :: Tc,phi,Tcx,Tcy

    CHARACTER(len=256)           :: message
    REAL                         :: R0,C_t,C_p,ell0,Depth,Area,L
    REAL                         :: hhl,D,fact,Nturb,Zmod
    REAL                         :: Km1,Km2,Km,U1,U2,U0

    INTEGER                      :: i,ii,j,jj,k,n,                     &
         &                          ITF,JTF,KTF,indi,ind,tpc,          &
         &                          iturbine,tnr,icell

    !Actually module is outside of the tile loop, use only K component
    JTF=MIN0(JTE,JDE-n1)
    KTF=MIN0(KTE,KDE-n1)
    ITF=MIN0(ITE,IDE-n1)

    !index = 1

    ! First time step only allocation, because we have to allocate only once
    ! then from the next timestep onwards, all tiles have to be used.
    !=========================================================================
    !This is just a way to do it. We go from cell to cell and at the end apply 
    !a thurst to it. 

    CELL:DO icell = n1,ncells
       
       i   = cells(icell)%i 
       j   = cells(icell)%j
       tpc = cells(icell)%turb_p_cell 


       write(message,*)'cell ',i,j,' I:',ITS,ITF,' J:',JTS,JTF,tpc,R0frac
       CALL wrf_debug(200,message)

       IF((ITS.LE.I.AND.I.LE.ITF).AND.(JTS.LE.J.AND.J.LE.JTF))THEN

          phi(:) = atan2(v(i,:,j),u(i,:,j)) + z2 * piGr
          phi(:) = mod(phi,z2 * piGr)


          !=====================================================================
          !We manipulate the i,j field where the turbines are located, with help 
          !of the reference field ii,jj. We assume therefore a local homogeneous 
          !mean velocity field (open terrain)
          Tc           = z0
          Tcx          = z0
          Tcy          = z0
          Tmeas(i,j)   = z0

          TURBINE:DO iturbine = n1,tpc

             tnr = cells(icell)%turb_nr(iturbine) 
             Nturb  = turbines(tnr)%Nturbs 
             hhl    = turbines(tnr)%hubheight
             R0     = z1d2 * turbines(tnr)%diameter

             ell0  = R0frac * R0
             Depth = dx
             Area  = dx*dx
             L     = z1d2 * dx

             k=1
             Zmod = Z(i,k,j) - HGT(i,j)
             do while(Zmod<hhl)
                k = k + 1
                Zmod = Z(i,k,j) - HGT(i,j)
                ind = k
             end do

             U1 = sqrt(U(i,ind-1,j)**n2 + V(i,ind-1,j)**n2)
             U2 = sqrt(U(i,ind  ,j)**n2 + V(i,ind  ,j)**n2)
   
             U0 =   U1 + (U2 - U1) * (log(hhl)                 - log(Z(i,ind-1,j)-hgt(i,j) ))&
                &                  / (log(Z(i,ind,j)-hgt(i,j)) - log(Z(i,ind-1,j)-hgt(i,j) ))
 
             Km1 = K_m(i,ind-1,j)
             Km2 = K_m(i,ind,j)
   
             Km =   Km1 + (Km2 - Km1) * (hhl - Z(i,ind-1,j)-hgt(i,j) )&
                &                     / (Z(i,ind,j) - Z(i,ind-1,j) )

!!!          =========  GET C_t & C_p  ===================================================
             IF(trim(turb_type).eq.'none')then !constant Ct assumed

                if(    U0.gt.turbines(tnr)%cutinspeed  .and.&
                     & U0.lt.turbines(tnr)%cutoutspeed )then
                   C_t = 0.89 !turbines(tnr)%Ct(n1)
                   C_p = Betz
                else
                   C_t = turbines(tnr)%Ct_stat
                   C_p = z0
                end if
             
             ELSE  ! C_t from specifications
             
                indi    = n1
                do while(U0.ge.turbines(tnr)%Ut(indi))
                   if(indi.ge.nCt)goto 110
                   indi = indi + n1
                end do
110             continue
                indi    = indi - n1

                !linear interpolation:
                if(    U0.gt.turbines(tnr)%cutinspeed .and.&
                     & U0.lt.turbines(tnr)%cutoutspeed )then
                   C_t    =  (U0 - turbines(tnr)%Ut(indi)) &
                        & * (turbines(tnr)%Ct(indi+n1) - turbines(tnr)%Ct(indi))  &
                        & / (turbines(tnr)%Ut(indi+n1) - turbines(tnr)%Ut(indi))  & 
                        & +  turbines(tnr)%Ct(indi)

                   C_p    =  (U0 - turbines(tnr)%Ut(indi)) &
                        & * (turbines(tnr)%Cp(indi+n1) - turbines(tnr)%Cp(indi))  &
                        & / (turbines(tnr)%Ut(indi+n1) - turbines(tnr)%Ut(indi))  & 
                        & +  turbines(tnr)%Cp(indi)
                else
                   C_t = turbines(tnr)%Ct_stat
                   C_p = z0
                end if


             END IF
!!!          =========  END GET C_t  ====================================================


             fact = z2 * max(Km,KMmin) / max(U0,eps)

             Tmeas(i,j) = Tmeas(i,j) &
                + z1d2 * rhoA * C_p * piGr * R0**2 * U0**3

             CALL WAKE(ks=KTS              , ke=KTF               ,&
                   &   U0m=U0              , hhl=hhl              ,&
                   &    R0=R0              , Area=Area            ,&
                   &    Z=Z(i,KTS:KTF,j)   , L=L                  ,&
                   &    out_turb=out_turb  , Nturb=Nturb          ,& 
                   &    Tc=Tc              , const=fact           ,&
                   &    ell0=ell0          , Depth=Depth          ,&
                   &    C_t=C_t            , hgt = hgt(i,j)        )

          END DO TURBINE

          !Per turbine would be:
          !Tmeas(i,j) = Tmeas(i,j) /REAL(tpc)

          !Eq. 11 and 12
          IF(TRIM(production).eq.'nowake')THEN
             dudt(i,:,j) = dudt(i,:,j)
             dvdt(i,:,j) = dvdt(i,:,j)
          ELSE
             dudt(i,:,j) = dudt(i,:,j) + cos(phi) * Tc
             dvdt(i,:,j) = dvdt(i,:,j) + sin(phi) * Tc
          END IF

       END IF

    END DO CELL

    write(message,*)'Max thrust:',maxval(Tmeas)
    CALL wrf_debug(200,message)




  END SUBROUTINE TURBINE_EWP
!!! #########################################################################
!!! ######################### END PUBLIC ROUTINES ###########################
!!! #########################################################################





!!! #########################################################################
!!! ######################### BEGIN LOCAL ROUTINES ##########################
!!! #########################################################################
!!! 
!!! =========================================================================
!!!                   Model Grid                           Thrust (Tc)
!!!                                                       0
!!!  |                                            |       | |  
!!!  |                                            |       | |
!!!  ----------------------------------------------       | ----      
!!!  |                    |                       |       |     |
!!!  |                    |                       |       |     |
!!!  --------------       | //////     ------------       |     --
!!!  |               hub  |--||                   |       |       |        
!!!  |                    |  ||                   |       |       |
!!!  --------------       |  ||        ------------       |     --
!!!  |                    |  ||                   |       |     |
!!!  |                       ||                   |       |     |
!!!  --------------          ||        ------------       | ----
!!!  |                       ||                   |       | |
!!!  |                       ||                   |       | |
!!! =========================================================================
!!! Turbine wakes defined around hub height and the description 
!!! uses the wnd speed and turbulence diffusion at this height 
!!! to determine the grid-cell averaged wake extension in the 
!!! vertical direction. Therefore, the grid-cell averaged 
!!! velocity deficit is not necessarily bounded to the turbine's 
!!! blade. 

!!! #########################################################################
!!!              calculate the deceleration for every turbine
!!! #########################################################################
  SUBROUTINE WAKE(ks,ke,U0m,hhl,R0,Area,Z,L,out_turb,Nturb,Tc  ,&
       &          const,ell0,Depth,C_t,hgt                      )
    
    IMPLICIT NONE
    
    REAL      ,DIMENSION(ks:ke)   ,INTENT(INOUT) :: Tc
    REAL                          ,INTENT(INOUT) :: hhl
    REAL      ,DIMENSION(ks:ke)   ,INTENT(IN   ) :: Z
    REAL                          ,INTENT(IN   ) :: L,R0,C_t,Area
    REAL                          ,INTENT(IN   ) :: ell0,Depth,hgt
    REAL                          ,INTENT(IN   ) :: const,U0m,Nturb
    INTEGER                       ,INTENT(IN   ) :: ks,ke
    INTEGER                       ,INTENT(IN   ) :: out_turb
    
    CHARACTER(len=256)          :: message 
    REAL                        :: Usmn,ellmn,Zhl
    REAL                        :: Xi
    INTEGER                     :: k,n



    !Eq. (8)
    ellmn = z2d3 * ( (const*L + ell0**2)**z3d2 - ell0**3) / (const*L)

    !Eq. (10)
    Usmn = C_t * sqrt_pi * R0**2 * U0m      & 
            &            / (sqrt8 * ellmn * Depth)

    !########################################################################
    !############### calculate the velocity deficit at each level ###########
    DO k=ks,ke-1

       !Inner part of Eq. (7)
       Xi = exp(-z1d2 * ( ( (Z(k)-hgt) - hhl) / ellmn)**n2)

       !Prepare Eq. 11 and 12
       Tc(k)     =  Tc(k)                                 &
           &     - Nturb * U0m * Usmn * Xi * Depth / Area

    END DO
    !########################################################################

    !if(out_turb.gt.100.and.tnr.lt.4)then
    !if(out_turb.gt.100)then
    !   write(message,*)'Tm ',tnr,U0m,ellmn,Usmn,C_t,const
    !   CALL wrf_message(message)
    !end if
        
  END SUBROUTINE WAKE 
!!! #########################################################################
!!! #########################################################################






!!! #########################################################################
!!! #########################################################################
!!! The input file where the locations of the individual turbines are defined, 
!!! will be read. The structure of the turbine will be alloc. and their
!!! elements be defined 
  SUBROUTINE PREP_TURB(dx,out_turb,known_lat,known_lon            ,&
                       ITS,JTS,KTS,KTF,IMS,IME,JMS,JME,config_flags)

    USE module_llxy

    IMPLICIT NONE

    !REAL   ,DIMENSION(KTS:KTF),INTENT(IN) :: zfull !dz    
    INTEGER,INTENT(IN)       :: ITS,JTS,KTS,KTF,IMS,IME,JMS,JME
    INTEGER,INTENT(IN)       :: out_turb
    REAL   ,INTENT(IN)       :: dx,known_lat,known_lon


    INTEGER                  :: ilon,ilat
    REAL                     :: diameter,cutinspeed,cutoutspeed,hubheight
    REAL                     :: Tlat,Tlon,frac,height
    REAL                     :: Lx,Ly,phi,Dist,nval

    
    INTEGER ,PARAMETER       :: ffarm = 19
    INTEGER                  :: n,m,i,j,ii,jj
    integer                  :: istat,dot,turbine_case
    character(len=256)       :: heather,message
    character(len=120)       :: inline,farmname
    character(len=20)        :: device
    logical                  :: rfile

    REAL :: ts_rx,ts_ry
    TYPE (PROJ_INFO) :: ts_proj
    TYPE (grid_config_rec_type) :: config_flags



    CALL nl_get_windturbines_spec( 1, farmname )

    dot = INDEX(TRIM(farmname),'.')    
    write(message,*)'rfile ',farmname!,dot
    CALL wrf_message(message)
    !#### DEFINE IF FILE TO READ ############################################
    IF     (farmname(dot+n1:dot+n5).eq.'real' ) THEN  
       turbine_case = WIND_TURBINES_REAL
       rfile    = .true.
    ELSE IF(farmname(dot+n1:dot+n5).eq.'ideal') THEN  
       turbine_case = WIND_TURBINES_IDEAL
       rfile    = .true.
    ELSE                                          
       turbine_case = WIND_TURBINES_OFF
       rfile        = .false.
    END IF
    !########################################################################
    

    istat = n0
      write(message,*)'rfile ',rfile,turbine_case ; CALL wrf_message(message)
    !########## READ FILE ###################################################
    readf:IF(rfile)THEN
       OPEN(file=TRIM(farmname),unit   = ffarm      ,&
            &                   FORM   = 'FORMATTED',&
            &                   STATUS = 'OLD'      ,&
            &                   IOSTAT = istat       )
       
       IF (istat .EQ. n0)THEN          
          ! get the number of turbines
          n = n0
          DO WHILE (.true.)
             READ(ffarm,'(A120)',END=30)inline
             write(message,*)inline; CALL wrf_message(message)
             IF ( INDEX(inline,'!')   .EQ. n0 .and. &
                  INDEX(inline,'#')   .EQ. n0)&
                  n = n + n1
          ENDDO
30        CONTINUE

          nturbines = n
          write(message,*)'nturbines ',nturbines; CALL wrf_message(message)
          !------  ALLOCATE TURBINES/MAST ------------------------------------
          IF(.not.ALLOCATED(turbines))ALLOCATE(turbines(nturbines))
          !-------------------------------------------------------------------
          REWIND(ffarm)
                    
          heather   = 'none'
          device    = 'none'

          n    = n1
          m    = n1
          nval = z0 

          !--------------------------------------------------------------
          !Decide which kind of data we have
          !heather should start with ### followed by one space
          READ(ffarm,'(A120)',END=120)inline
          if(inline(5:26).eq.'latlon')then
             heather = 'latlon'
          else if(inline(5:26).eq.'lonlat')then
             heather = 'lonlat'
          else !Default same as WRF-WF:
             heather = 'lonlat'
          end if

          DO WHILE (.true.)

             READ(ffarm,'(A120)',END=120)inline

             !---------------------------------------------------------------
             INFO:IF ( n .le. nturbines.and.INDEX(inline,'!').eq.n0 &
                  &                    .and.INDEX(inline,'#').eq.n0)THEN     
                
                IF(trim(heather).eq.'latlon')THEN
                   READ(inline,*,ERR=130)nval,Tlat,Tlon,device
                ELSE
                   READ(inline,*,ERR=130)nval,Tlon,Tlat,device
                END IF


                !read the corresponding specifications
                CALL GET_SPEC(n=n,cutinspeed=cutinspeed, &
                     &         cutoutspeed=cutoutspeed,  &
                     &         hubheight=hubheight,      &
                     &         diameter=diameter,        &
                     &           device=device           )

                !If real find IJ
                IF(turbine_case .eq. WIND_TURBINES_REAL)THEN 
                    !This can be updated for if we allow for lon lat positions

                   CALL map_init(ts_proj)

                   ! Mercator
                   IF (config_flags%map_proj == PROJ_MERC) THEN
                      CALL map_set(PROJ_MERC, ts_proj,     &
                         truelat1 = config_flags%truelat1, &
                         lat1     = known_lat,             &
                         lon1     = known_lon,             &
                         knowni   = REAL(its),             &
                         knownj   = REAL(jts),             &
                         dx       = dx)!config_flags%dx)

                   ! Lambert conformal
                   ELSE IF (config_flags%map_proj == PROJ_LC) THEN
                      CALL map_set(PROJ_LC, ts_proj,        &
                         truelat1 = config_flags%truelat1,  &
                         truelat2 = config_flags%truelat2,  &
                         stdlon   = config_flags%stand_lon, &
                         lat1     = known_lat,              &
                         lon1     = known_lon,              &
                         knowni   = REAL(its),              &
                         knownj   = REAL(jts),              &
                         dx       = dx)!config_flags%dx)
                   ! Polar stereographic
                   ELSE IF (config_flags%map_proj == PROJ_PS) THEN
                      CALL map_set(PROJ_PS, ts_proj,        &
                         truelat1 = config_flags%truelat1,  &
                         stdlon   = config_flags%stand_lon, &
                         lat1     = known_lat,              &
                         lon1     = known_lon,              &
                         knowni   = REAL(its),              &
                         knownj   = REAL(jts),              &
                         dx       = dx)!config_flags%dx)
                   END IF

                   CALL latlon_to_ij(ts_proj, Tlat, Tlon, ts_rx, ts_ry)

                   ii = FLOOR(ts_rx)
                   jj = FLOOR(ts_ry)

                   write(message,*)ii,jj; CALL wrf_message(message)

                ELSE ! We have the position in distance (km) from origin i=1,j=1
                   
                   ii = FLOOR(Tlon)
                   jj = FLOOR(Tlat)

                END IF

                turbines(n)%turb_nr     = n
                turbines(n)%i           = ii
                turbines(n)%j           = jj
                turbines(n)%Nturbs      = nval
                turbines(n)%hubheight   = hubheight
                turbines(n)%diameter    = diameter
                turbines(n)%cutinspeed  = cutinspeed
                turbines(n)%cutoutspeed = cutoutspeed

                n = n + n1
                                
             ENDIF INFO
             
          ENDDO
          
120       CONTINUE ; CLOSE(ffarm) ;                 GOTO 150 
130       CONTINUE ; CLOSE(ffarm) ; istat = error ; GOTO 150 
          
       ENDIF
       
    END IF readf
150 CONTINUE

    IF (istat.eq.error)CALL ABORT('error reading file ')



  END SUBROUTINE PREP_TURB
!!! #########################################################################
!!! #########################################################################







!!! #########################################################################
!!! #########################################################################
!!! Define the flow direction, then fill in all elements of the structure cells 
  SUBROUTINE PREP_CELL(kms,kme,ims,ime,jms,jme,JTS,JTF,ITS,ITF,KTS,KTF,dx)
    
    IMPLICIT NONE
    
    REAL                              ,INTENT(IN)::dx
    INTEGER                           ,INTENT(IN)::ims,ime,its,itf
    INTEGER                           ,INTENT(IN)::jms,jme,jts,jtf
    INTEGER                           ,INTENT(IN)::kms,kme,kts,ktf

    INTEGER                         :: iturbine,cnt,indi,indo,k
    INTEGER                         :: i,ii,j,jj,n,nn,o,si,fi,so,fo
    INTEGER                         :: mini,maxi,minj,maxj,hcnt
    LOGICAL                         :: find



    mini = large
    minj = large
    maxi = z0
    maxj = z0

    ! Get the mean wind direction
    DO iturbine = n1,nturbines
       
       i = turbines(iturbine)%i
       j = turbines(iturbine)%j
      
       IF(i.lt.mini)mini=i   ! 
       IF(i.gt.maxi)maxi=i   ! save the min and max coordinates
       IF(j.lt.minj)minj=j   ! of the turbines
       IF(j.gt.maxj)maxj=j   !
       
    END DO

    ncells = n0
    
    !-- count the number of cells containing turbines -----------------------
    DO i=mini,maxi,n1
       DO j=minj,maxj,n1
          
          find     = .false.
          iturbine = n1
          
          DO WHILE(.not.find.AND.iturbine.le.nturbines)
             
             ii = turbines(iturbine)%i 
             jj = turbines(iturbine)%j
             
             IF(ii.eq.i.and.jj.eq.j)THEN
                ncells = ncells + n1
                find   = .true.
             END IF
             
             iturbine = iturbine + n1
             
          END DO
          
       END DO
    END DO

    !------------------------------------------------------------------------
    !------  ALLOCATE cells and dummy cnt -----------------------------------
    IF(.not.ALLOCATED(cells  ))ALLOCATE(  cells(ncells))
    IF(.not.ALLOCATED(TPC_CNT))ALLOCATE(TPC_CNT(ncells))
    !------------------------------------------------------------------------
    !------------------------------------------------------------------------

    indo = -1        ; indi =  1   ! N-S (-j) .. W-E ( i)
    so   = maxj      ; fo   = minj
    si   = mini      ; fi   = maxi

    n  = n1 !1st cells index
    nn = n1 !2nd cells index
    !------------------------------------------------------------------------
    !- Find the number of turbines that are in each cell                    -
    !- Allocate component of cells%...                                      -
    !- give the turbine numbers to the cells                                -
    DO o=so,fo,indo    !o=outer
       DO i=si,fi,indi !i=inner
          !------------------------------------------------------------------
          cnt = n0 ! cnt number of turbines in a cell
          DO iturbine = n1,nturbines
             
             ii = turbines(iturbine)%i 
             jj = turbines(iturbine)%j
             
             !found a turbine in i,j:
             IF(ii.EQ.i.AND.jj.EQ.o)cnt = cnt + n1

             IF(iturbine.EQ.nturbines.and.cnt.gt.n0)THEN
                
                !------  ALLOCATE -------------------------------------------
                IF(.not.ALLOCATED(cells(n)%turb_nr    ))&
                     & ALLOCATE  (cells(n)%turb_nr(cnt))
                !------------------------------------------------------------
                cells(n)%turb_p_cell = cnt
                TPC_CNT(n)           = cnt

                n = n + n1
                
             END IF
             
          END DO

          !------------------------------------------------------------------
          cnt = n1 ! index
          DO iturbine = n1,nturbines
             
             ii = turbines(iturbine)%i 
             jj = turbines(iturbine)%j

             IF(ii.EQ.i.AND.jj.EQ.o)THEN

                IF(cnt.eq.n1)THEN
                   cells(nn)%i = ii
                   cells(nn)%j = jj
                END IF

                hcnt = n1
                do k=n1,nturbines
                   if(turbines(k)%i.eq.ii.and.turbines(k)%j.eq.jj)then
                      if(hcnt.eq.cnt)cells(nn)%turb_nr(cnt) = turbines(k)%turb_nr
                      hcnt = hcnt + n1
                   end if
                end do

                cnt = cnt + n1
             END IF

             !found at least a turbine, then 
             !we have to go to the next cell index
             IF(cnt.GT.n1.AND.iturbine.EQ.nturbines)&
                  nn = nn + n1
          END DO
          !------------------------------------------------------------------
          
       END DO
    END DO
!    END IF

  END SUBROUTINE PREP_CELL
!!! #########################################################################
!!! #########################################################################



  



!!! #########################################################################
!!! #########################################################################
!!! At the moment every turbine will have its own Ct curve, which is quiet 
!!! a wast of memory,. Give him a name and define one Ct curve where it 
!!! will be connected to. The actual situation has the advantage that in 
!!! principle every turbine could behave different
  SUBROUTINE GET_SPEC(n,cutinspeed,cutoutspeed,hubheight,diameter,device)
    
    IMPLICIT NONE
    
    INTEGER           ,INTENT(IN)    :: n !turbines(n)
    REAL              ,INTENT(INOUT) :: cutinspeed,cutoutspeed,hubheight,diameter
    REAL                             :: b,R2_d_R02

    character(len=20)                :: device
    character(len=256)               :: message
    CHARACTER(len=120)               :: specname,inline,spec
    INTEGER ,PARAMETER               :: fspec = 98
    INTEGER                          :: istat,ih,is,i
    REAL                             :: val
    
    
    istat = n0
    
    !Constant Ct
    IF(trim(device).eq.'none')then
       
       b = z2d3
       
       turbines(n)%Ct(n1)  =  b * (z2 - b) 
       turbines(n)%Ct_stat =  0.05
       
       cutinspeed          = cutinspeed
       cutoutspeed         = cutoutspeed
       hubheight           = hubheight
       diameter            = diameter
       nCt                 = n1

    ELSE 
       
       write(specname,'(A,A8)')trim(device),'.turbine'
       
       OPEN(file=TRIM(specname),unit   = fspec      ,&
            &                   FORM   = 'FORMATTED',&
            &                   STATUS = 'OLD'      ,&
            &                   IOSTAT = istat       )
       
       IF (istat .EQ. n0)THEN
          
          ! get the number of turbines
          ih = n0  !heather
          is = n0  !specification
          DO WHILE (.true.)
             READ(fspec,'(A120)',END=30)inline
             IF ( INDEX(inline,'!')   .EQ. n0)THEN
                is = is + n1
             ELSE
                ih = ih + n1
             END IF
          ENDDO
30        CONTINUE
          
          nCt = is
          
          ALLOCATE(turbines(n)%Ut(nCt))
          ALLOCATE(turbines(n)%Ct(nCt))
          ALLOCATE(turbines(n)%Cp(nCt))
          
          REWIND(fspec)
          
          DO i=n1,ih
             READ(fspec,*)spec,val
             SELECT CASE(trim(spec))
             CASE('!cutinspeed' );cutinspeed          = val
             CASE('!cutoutspeed');cutoutspeed         = val
             CASE('!hubheight'  );hubheight           = val
             CASE('!diameter'   );diameter            = val
             CASE('!Ct_stat'    );turbines(n)%Ct_stat = val
             END SELECT
          END DO
          
          !DO i=n1,nCt
          i = n1
          DO WHILE (.true.)
             READ(fspec,*,END=40)&
                   turbines(n)%Ut(i),turbines(n)%Ct(i),turbines(n)%Cp(i)
 
             if(turbines(n)%Cp(i).gt.n1)then
                turbines(n)%Cp(i) = turbines(n)%Cp(i) / &
                 (z1d2 * rhoA *piGr * (z1d2*diameter)**2 * turbines(n)%Ut(i)**3)
             end if

             i = i + n1

          END DO
 40       CONTINUE
          
          CLOSE(fspec)
          
       ELSE
          
          write(specname,'(A16,A,A15)')&
               'No turbine file ',trim(specname),' could be found'
          
          CALL ABORT(trim(specname))
          
       END IF
       
    END IF

  END SUBROUTINE GET_SPEC
!!! #########################################################################
!!! #########################################################################



!!! #########################################################################
!!! #########################################################################
  SUBROUTINE ABORT(text)
    
    IMPLICIT NONE
    

    CHARACTER(len=*)        :: text
    
    write(*,'(A)')trim(text); RETURN

    
  END SUBROUTINE ABORT
!!! #########################################################################
!!! #########################################################################



!!! #########################################################################
!!! ######################### END LOCAL ROUTINES ############################
!!! #########################################################################

END MODULE module_wf_ewp
